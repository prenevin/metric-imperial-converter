package com.organisation.subdomain.teamname.metricimperialconverter.web.advice;

import com.organisation.subdomain.teamname.metricimperialconverter.exception.ClientException;
import com.organisation.subdomain.teamname.metricimperialconverter.exception.SystemException;
import com.organisation.subdomain.teamname.metricimperialconverter.web.view.ErrorView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ControllerAdviceErrorHandling {


    @ExceptionHandler(ClientException.class)
    public ResponseEntity<ErrorView> clientExceptionHandler(final ClientException clientException) {
        if (log.isDebugEnabled()) {
            // Demo - This is on debug on purpose. All client Exceptions are part of the design (given poor data, respond with a defined ErrorView)
            log.debug("Client Error handling: ", clientException);
        }

        final String message = clientException.getMessage();
        final String code = clientException.getErrorCode().getCode();
        return new ResponseEntity<>(new ErrorView(message, code), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SystemException.class)
    protected ResponseEntity<ErrorView> unforeseenException(final SystemException ex) {
        log.error("Unforeseen system exception has occurred", ex);
        // Log it to SLF4J and not just System.out.print, a proper logging allows for log messages to be formatted(so that logs are consistent with time),
        // filtered by log level(root level or even logger level) and multiple appender/target locations can be defined
        // and configured (If tomorrow we want to start logging to FluentD it as easy as configuring a new appender to the logging framework)
        return new ResponseEntity<>(new ErrorView("Internal Server error", "0000"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
