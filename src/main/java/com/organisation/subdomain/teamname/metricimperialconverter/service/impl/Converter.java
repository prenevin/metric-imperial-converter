package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;

import java.math.BigDecimal;

public interface Converter {


    UnitOfMeasurement getTargetUnitOfMeasurement();

    Conversion convert(BigDecimal amount);

}
