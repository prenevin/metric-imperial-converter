package com.organisation.subdomain.teamname.metricimperialconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetricImperialConverterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MetricImperialConverterApplication.class, args);
    }

}
