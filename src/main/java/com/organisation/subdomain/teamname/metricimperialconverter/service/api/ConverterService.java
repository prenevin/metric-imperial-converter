package com.organisation.subdomain.teamname.metricimperialconverter.service.api;

import java.math.BigDecimal;

public interface ConverterService {
    Conversion convert(BigDecimal amount, UnitOfMeasurement targetUnitOfMeasurement);
}
