package com.organisation.subdomain.teamname.metricimperialconverter.service.api;

public enum UnitOfMeasurement {
    // Liquid Volumes
    LITRE,
    PINT,

    // Temperature
    CELSIUS,
    FAHRENHEIT,

    // Mass
    KILOGRAM,
    POUND;

}
