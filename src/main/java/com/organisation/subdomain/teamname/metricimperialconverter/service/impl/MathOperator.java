package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

public enum MathOperator {
    MULTIPLY,
    DIVIDE;

}
