package com.organisation.subdomain.teamname.metricimperialconverter.web.view;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;
import lombok.Data;

@Data
public class ConversionResponse {
    private final String value;
    private final String unitOfMeasurement;

    public ConversionResponse(final String value, final String unitOfMeasurement) {
        this.value = value;
        this.unitOfMeasurement = unitOfMeasurement;
    }

    public String getValue() {
        return value;
    }

    public String getUnitOfMeasurement() {
        return unitOfMeasurement;
    }
}
