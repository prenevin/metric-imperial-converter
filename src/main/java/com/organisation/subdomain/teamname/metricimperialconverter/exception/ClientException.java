package com.organisation.subdomain.teamname.metricimperialconverter.exception;


import lombok.Getter;

/**
 * User as the parent exception for all client related errors such as bad input
 */
public class ClientException extends RuntimeException {
    @Getter // Demo - I just want a .getErrorCode method.
    // Opting not to use the @Data as I don't want getters, setters, toString, hashcode and equals for this class
    private final ErrorCode errorCode;

    public ClientException(final ErrorCode errorCode, Object... args) {
        super(String.format(errorCode.getExceptionMessage(), args));
        this.errorCode = errorCode;
    }

    public ClientException(final ErrorCode errorCode, final Throwable ex, Object... args) {
        super(String.format(errorCode.getExceptionMessage(), args), ex);
        this.errorCode = errorCode;
    }

    /**
     * Every exception in the application should be handled.
     * Either by catching and handling the error or wrap the exception into an excpetion defined by our application
     *
     * This ErrorCode enum allows us to define a stand set of error codes that this exception can handle.
     * It also makes debugging easier as every exception must have an error code. By using our IDE we can come straight to this
     * enum and find all the place this exception with this error code could be thrown from
     * (Given an error code I know exactly which line of code is at fault)
     */
    public enum ErrorCode {
        INVALID_AMOUNT_CONVERSION_REQUEST("CON-1001", "Invalid amount provided '%s'"),
        INVALID_UNIT_OF_MEASUREMENT_CONVERSION_REQUEST("CON-1002", "Invalid targetUnitOfMeasurement provided '%s'");

        private final String code;
        private final String exceptionMessage;

        ErrorCode(final String code, final String exceptionMessage) {
            this.code = code;
            this.exceptionMessage = exceptionMessage;
        }

        public String getCode() {
            return code;
        }

        public String getExceptionMessage() {
            return exceptionMessage;
        }
    }

}
