package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class TemperatureConverter implements Converter {
    // Demo these can be constants as BigDecimal and BigInteger are immutable.
    // So we can create it one when the class is loaded from the classpath into the JVM
    // TODO-pren before submitting maybe move all the BigDecimal Constants to a Constants class (final and private constructor)
    public static final BigDecimal FIVE = BigDecimal.valueOf(5L);
    public static final BigDecimal NINE = BigDecimal.valueOf(9L);
    public static final BigDecimal THIRTY_TWO = BigDecimal.valueOf(32);

    private final UnitOfMeasurement targetUnitOfMeasurement;

    public TemperatureConverter(final UnitOfMeasurement targetUnitOfMeasurement) {
        this.targetUnitOfMeasurement = targetUnitOfMeasurement;
    }

    @Override
    public UnitOfMeasurement getTargetUnitOfMeasurement() {
        return targetUnitOfMeasurement;
    }

    @Override
    public Conversion convert(final BigDecimal amount) {
        if (UnitOfMeasurement.CELSIUS.equals(targetUnitOfMeasurement)) {
            BigDecimal fahrenheit = amount.divide(FIVE, MathContext.DECIMAL128).multiply(NINE).add(THIRTY_TWO);
            return new Conversion(fahrenheit, UnitOfMeasurement.FAHRENHEIT);
        } else { // Can only be UnitOfMeasurement.FAHRENHEIT
            BigDecimal celsius = amount.subtract(THIRTY_TWO).divide(NINE,MathContext.DECIMAL128).multiply(FIVE);
            return new Conversion(celsius, UnitOfMeasurement.CELSIUS);
        }
    }
}
