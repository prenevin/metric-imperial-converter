package com.organisation.subdomain.teamname.metricimperialconverter.web.view;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Data
public class ConversionRequest {

    private String amount;

    /**
     * The target UnitOfMeasurement implies the sources.
     * Example: when the targetUnitOfMeasurement is "LITRE" then the source is amount implies "PINT"
     */
    private String targetUnitOfMeasurement;


    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("amount", amount)
            .append("targetUnitOfMeasurement", targetUnitOfMeasurement)
            .toString();
    }
}
