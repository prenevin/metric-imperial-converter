package com.organisation.subdomain.teamname.metricimperialconverter.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "metricimperialconverter")
@NoArgsConstructor
public class SimpleConvertersConfig {

    @Getter
    @Setter
    List<SimpleConverterEntry> simpleConverterEntries;

}
