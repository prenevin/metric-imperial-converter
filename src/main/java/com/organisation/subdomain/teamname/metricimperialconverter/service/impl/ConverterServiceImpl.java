package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.ConverterService;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ConverterServiceImpl implements ConverterService {

    private final ConverterFactory converterFactory;

    public ConverterServiceImpl(final ConverterFactory converterFactory) {
        this.converterFactory = converterFactory;
    }

    @Override
    public Conversion convert(final BigDecimal amount, final UnitOfMeasurement targetUnitOfMeasurement) {
        Converter converter = converterFactory.getConverter(targetUnitOfMeasurement);
        return converter.convert(amount);
    }
}
