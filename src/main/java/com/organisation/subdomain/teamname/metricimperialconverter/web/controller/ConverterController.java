package com.organisation.subdomain.teamname.metricimperialconverter.web.controller;

import com.organisation.subdomain.teamname.metricimperialconverter.exception.ClientException;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.ConverterService;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;
import com.organisation.subdomain.teamname.metricimperialconverter.web.view.ConversionRequest;
import com.organisation.subdomain.teamname.metricimperialconverter.web.view.ConversionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * Demo - The responsibility of the Controller is to manage the incoming request from the client
 * This means that the controller lives as independently as possible from the service layer
 * The service layer is for managing the business transaction agnostic from how the data enters the system (in this case only HTTP-REST)
 */
@RestController
@RequestMapping("generic-conversion")
@Slf4j
public class ConverterController {

    private final ConverterService conversionService;

    public ConverterController(final ConverterService conversionService) {
        this.conversionService = conversionService;
    }

    @PostMapping
    public ResponseEntity<ConversionResponse> convert(@RequestBody ConversionRequest conversionRequest) {
        // Demo - wrap all log levels below "INFO" in this style of if statement. Log methods do execute, even if they are not shown in the console(on of the configured appenders)
        if (log.isDebugEnabled()) {
            log.debug("Request to convert=<{}>", conversionRequest);
        }

        // Demo - Show client error handling without Bean validation(JSR 303). Show ControllerAdvice where you now handle the propagated exception
        final BigDecimal amount = deserialiseAmount(conversionRequest);
        final UnitOfMeasurement unitOfMeasurement = deserialiseUnitOfMeasurement(conversionRequest);

        final Conversion convert = conversionService.convert(amount, unitOfMeasurement);

        final ConversionResponse conversionResponse = new ConversionResponse(convert.getValue().toString(), convert.getUnitOfMeasurement().toString());
        return new ResponseEntity<>(conversionResponse, HttpStatus.OK);
    }

    private UnitOfMeasurement deserialiseUnitOfMeasurement(final ConversionRequest conversionRequest) {
        try {
            return UnitOfMeasurement.valueOf(conversionRequest.getTargetUnitOfMeasurement());
        } catch (IllegalArgumentException e) {
            throw new ClientException(ClientException.ErrorCode.INVALID_UNIT_OF_MEASUREMENT_CONVERSION_REQUEST, conversionRequest.getTargetUnitOfMeasurement());
        }
    }


    private BigDecimal deserialiseAmount(final ConversionRequest conversionRequest) {
        try {
            return new BigDecimal(conversionRequest.getAmount());
        } catch (NumberFormatException e) {
            throw new ClientException(ClientException.ErrorCode.INVALID_AMOUNT_CONVERSION_REQUEST, e, conversionRequest.getAmount());
        }
    }
}
