package com.organisation.subdomain.teamname.metricimperialconverter.exception;


/**
 * User as the parent exception for all system related errors such as a Database connection failing or in this case the
 * target FileSystem not being available.
 * (Now how to demo this to mimic a 500 HTTP error. Will be great if I can do a 4xx and 5xx demoing the difference between client and system)
 */
public class SystemException extends RuntimeException {
    public SystemException() {
    }

    public SystemException(final String message) {
        super(message);
    }

    public SystemException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public SystemException(final Throwable cause) {
        super(cause);
    }

    public SystemException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
