package com.organisation.subdomain.teamname.metricimperialconverter.web.view;

import lombok.Data;

@Data
public class ErrorView {

    private final String message;

    private final String code;

    public ErrorView(final String message, final String code) {
        this.message = message;
        this.code = code;
    }
}
