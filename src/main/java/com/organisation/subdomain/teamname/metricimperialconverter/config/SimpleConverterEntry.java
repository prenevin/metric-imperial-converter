package com.organisation.subdomain.teamname.metricimperialconverter.config;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;
import com.organisation.subdomain.teamname.metricimperialconverter.service.impl.MathOperator;

import java.math.BigDecimal;

public class SimpleConverterEntry {
    private BigDecimal ratio;
    private MathOperator operator;
    private UnitOfMeasurement targetUnitOfMeasurement;
    private UnitOfMeasurement sourceUnitOfMeasurement;

    public SimpleConverterEntry() {
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(final BigDecimal ratio) {
        this.ratio = ratio;
    }

    public MathOperator getOperator() {
        return operator;
    }

    public void setOperator(final MathOperator operator) {
        this.operator = operator;
    }

    public UnitOfMeasurement getTargetUnitOfMeasurement() {
        return targetUnitOfMeasurement;
    }

    public void setTargetUnitOfMeasurement(final UnitOfMeasurement targetUnitOfMeasurement) {
        this.targetUnitOfMeasurement = targetUnitOfMeasurement;
    }

    public UnitOfMeasurement getSourceUnitOfMeasurement() {
        return sourceUnitOfMeasurement;
    }

    public void setSourceUnitOfMeasurement(final UnitOfMeasurement sourceUnitOfMeasurement) {
        this.sourceUnitOfMeasurement = sourceUnitOfMeasurement;
    }
}
