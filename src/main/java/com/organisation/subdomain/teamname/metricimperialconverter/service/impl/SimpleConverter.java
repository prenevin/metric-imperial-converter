package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.exception.SystemException;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class SimpleConverter implements Converter {
    private final BigDecimal ratio;
    private final MathOperator operator;
    private final UnitOfMeasurement targetUnitOfMeasurement;

    public SimpleConverter(final BigDecimal ratio, final MathOperator operator, final UnitOfMeasurement targetUnitOfMeasurement) {
        this.ratio = ratio;
        this.operator = operator;
        this.targetUnitOfMeasurement = targetUnitOfMeasurement;
    }

    @Override
    public UnitOfMeasurement getTargetUnitOfMeasurement() {
        return targetUnitOfMeasurement;
    }

    @Override
    public Conversion convert(final BigDecimal amount) {
        final BigDecimal result;
        if (MathOperator.MULTIPLY.equals(operator)) {
            result = amount.multiply(ratio);
            return new Conversion(result, targetUnitOfMeasurement);
        } else if (MathOperator.DIVIDE.equals(operator)) {
            result = amount.divide(ratio, MathContext.DECIMAL128).setScale(3, RoundingMode.HALF_UP);
            return new Conversion(result, targetUnitOfMeasurement);
        }
        throw new SystemException(String.format("Unexpected Enum value=<%s> for enum type 'MathOperator'", operator));
    }
}
