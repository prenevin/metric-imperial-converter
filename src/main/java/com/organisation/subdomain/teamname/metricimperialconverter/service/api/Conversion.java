package com.organisation.subdomain.teamname.metricimperialconverter.service.api;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Conversion {
    private final BigDecimal value;
    private final UnitOfMeasurement unitOfMeasurement;

    public Conversion(final BigDecimal value, final UnitOfMeasurement unitOfMeasurement) {
        this.value = value;
        this.unitOfMeasurement = unitOfMeasurement;
    }
}
