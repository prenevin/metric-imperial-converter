package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.config.SimpleConverterEntry;
import com.organisation.subdomain.teamname.metricimperialconverter.config.SimpleConvertersConfig;
import com.organisation.subdomain.teamname.metricimperialconverter.exception.SystemException;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class ConverterFactory {


    private final Map<UnitOfMeasurement, SimpleConverter> simpleConverters;

    public ConverterFactory(final SimpleConvertersConfig simpleConvertersConfig) {
        final Map<UnitOfMeasurement, SimpleConverter> setupMap = new HashMap<>();

        // TODO-Demo - Leaving commented code here so show evolution and refactoring (I should have planned better and used git commits to show refactoring and improvement)

//        setupMap.put(UnitOfMeasurement.LITRE, new SimpleConverter(new BigDecimal("2.113"), MathOperator.MULTIPLY, UnitOfMeasurement.PINT));
//        setupMap.put(UnitOfMeasurement.PINT, new SimpleConverter(new BigDecimal("2.113"), MathOperator.DIVIDE, UnitOfMeasurement.LITRE));
//        setupMap.put(UnitOfMeasurement.KILOGRAM, new SimpleConverter(new BigDecimal("2.205"), MathOperator.MULTIPLY, UnitOfMeasurement.POUND));
//        setupMap.put(UnitOfMeasurement.POUND, new SimpleConverter(new BigDecimal("2.205"), MathOperator.DIVIDE, UnitOfMeasurement.KILOGRAM));

        for (final SimpleConverterEntry s : simpleConvertersConfig.getSimpleConverterEntries()) {
            setupMap.put(s.getSourceUnitOfMeasurement(), new SimpleConverter(s.getRatio(), s.getOperator(), s.getTargetUnitOfMeasurement()));
        }

        // Could have just created a map straight from config, but it reads better with the source and target Units of measurement
        simpleConverters = Collections.unmodifiableMap(setupMap);
    }

    public Converter getConverter(final UnitOfMeasurement targetUnitOfMeasurement) {
        // Demo - equals on enum constant is null safe (See java.lang.Object For any non-null reference value x, x.equals(null) should return false)
        if (UnitOfMeasurement.CELSIUS.equals(targetUnitOfMeasurement) || UnitOfMeasurement.FAHRENHEIT.equals(targetUnitOfMeasurement)) {
            return new TemperatureConverter(targetUnitOfMeasurement);
        } else if (simpleConverters.containsKey(targetUnitOfMeasurement)) {
            return simpleConverters.get(targetUnitOfMeasurement);
        }
        // Don't return null, rather throw an exception explaining the issue. This error is with the implementation and not the calling client
        throw new SystemException(String.format("Unexpected Enum value=<%s> for enum type 'UnitOfMeasurement'", targetUnitOfMeasurement));
    }
}
