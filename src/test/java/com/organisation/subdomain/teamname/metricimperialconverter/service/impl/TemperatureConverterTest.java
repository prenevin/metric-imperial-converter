package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class TemperatureConverterTest {

    @Test
    void testGivenNegative40CelsiusThenConvertToFahrenheitShouldBe40() {
        // Given
        final TemperatureConverter temperatureConverter = new TemperatureConverter(UnitOfMeasurement.CELSIUS);

        // When
        final Conversion convert = temperatureConverter.convert(new BigDecimal(-40));

        // Then
        assertThat(convert.getValue()).isNotNull().isEqualTo(new BigDecimal(-40)); // -40 is where they are the same
        assertThat(convert.getUnitOfMeasurement()).isNotNull().isEqualByComparingTo(UnitOfMeasurement.FAHRENHEIT);
    }

    @Test
    void testGiven104FahrenheitThenConvertToCelsiusShouldBe40() {
        // Given
        final TemperatureConverter temperatureConverter = new TemperatureConverter(UnitOfMeasurement.FAHRENHEIT);

        // When covert 104 FAHRENHEIT
        final Conversion convert = temperatureConverter.convert(new BigDecimal(104));

        // Then CELSIUS should be 40
        assertThat(convert.getValue()).isNotNull().isEqualTo(new BigDecimal(40)); // -40 is where they are the same
        assertThat(convert.getUnitOfMeasurement()).isNotNull().isEqualByComparingTo(UnitOfMeasurement.CELSIUS);
    }
}
