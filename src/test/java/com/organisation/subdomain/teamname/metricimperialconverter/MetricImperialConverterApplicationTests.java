package com.organisation.subdomain.teamname.metricimperialconverter;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MetricImperialConverterApplicationTests {

    @Test
    void contextLoads() {
        // This test might look empty, but it does test if the Spring (DI container) will start up and all dependencies are satisfied
    }

}
