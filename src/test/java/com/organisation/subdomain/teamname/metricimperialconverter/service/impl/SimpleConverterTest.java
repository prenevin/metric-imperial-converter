package com.organisation.subdomain.teamname.metricimperialconverter.service.impl;

import com.organisation.subdomain.teamname.metricimperialconverter.service.api.Conversion;
import com.organisation.subdomain.teamname.metricimperialconverter.service.api.UnitOfMeasurement;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


class SimpleConverterTest {

    // Source(https://www.google.com/search?q=litre+to+pint)  Formula for an approximate result, multiply the volume value by 2,113
    @Test
    void testLiterToUsPint() {
        // Given
        final SimpleConverter simpleConverter = new SimpleConverter(new BigDecimal("2.113"), MathOperator.MULTIPLY, UnitOfMeasurement.PINT);

        // When
        final Conversion convert = simpleConverter.convert(BigDecimal.valueOf(2));

        //Then
        assertThat(convert.getValue()).isNotNull().isEqualByComparingTo(new BigDecimal("4.226"));
        assertThat(convert.getUnitOfMeasurement()).isNotNull().isEqualByComparingTo(UnitOfMeasurement.PINT);
    }

    // Source(https://www.google.com/search?q=pint+to+litre)  Formula for an approximate result, divide the volume value by 2,113
    @Test
    void testUsPintToLiter() {
        // Given
        final SimpleConverter simpleConverter = new SimpleConverter(new BigDecimal("2.113"), MathOperator.DIVIDE, UnitOfMeasurement.LITRE);

        // When
        final Conversion convert = simpleConverter.convert(BigDecimal.valueOf(8));

        //Then
        assertThat(convert.getValue()).isNotNull().isEqualByComparingTo(new BigDecimal("3.786"));
        assertThat(convert.getUnitOfMeasurement()).isNotNull().isEqualByComparingTo(UnitOfMeasurement.LITRE);
    }
}
