# Metric Imperial Converter

This application converts Temperature, Mass and Volume
The goal of this project is to demo some features of Spring boot and general java project design.


## Requirements

For building the application you will need:
* Maven 3
* Java 11

## Running the application locally
```shell
mvn spring-boot:run
```

## Notes

* Jmeter project file is under jmeter directory with a bat file(jmeter-runner.bat) to help run the plan without the GUI
* Dockerfile is in the root of this project with a bat file(run-docker-build.bat) to demo how to build the image and run a single container called "pren-test" and expose the endpoint on port 8080.
* TODO integrate Jmeter test with maven. Start up the application, then run jmeter test plan, if any cases fail then we fail the build
