FROM maven:3.6.2-jdk-11-slim AS MAVEN_BUILD

COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean package
# Note above I don't need to specify "compile", "test-compile", "test" since those phases are required to run for "package"
# "clean" phase on the other is not part of the "default" Lifecycle but is part of its own "clean" Lifecycle. (There is the site lifecycle but I've never seen it used yet)

FROM openjdk:11-jre
# Note we don't need the entire jdk(Java Development Kit which contains the compiler and other tools for building java
# apps), we just need the JRE (Java Runtime Environment) to run our app since its already compiled and ready to run
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/*.jar /app/app.jar
EXPOSE 8080/tcp
ENTRYPOINT ["java", "-jar", "app.jar"]

# For PROD we can set dynamically set the profile using the environment variables ("SPRING_PROFILES_ACTIVE").
# If this docker image were to run in a kubernetes environment we can set the Environment Variable in the service and every pod will then have that variable
